<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('iniciativa/list', 'IniciativaController@list');
Route::get('iniciativa/list/{id}', 'IniciativaController@list');
Route::post('iniciativa/save', 'IniciativaController@save');
Route::get('iniciativa/delete/{id}', 'IniciativaController@delete');