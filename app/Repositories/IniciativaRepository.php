<?php
 
namespace App\Repositories;

use App\Models\Iniciativa;
 
class IniciativaRepository 
{
 
	public function findAll()
	{
		return Iniciativa::all();
	}

	public function findById($id) {
		return Iniciativa::find($id);
	}   

	public function create($data) {
		$iniciativa = new Iniciativa();
		foreach ($data as $key => $value) {
			$iniciativa->$key = $value;
		}
		$iniciativa->save();
		return $iniciativa;
	}

	public function update($id,$data) {
		$iniciativa = $this->findById($id);
		if ($iniciativa) {
			foreach ($data as $key => $value) {
				$iniciativa->$key = $value;
			}
			$iniciativa->save();
			return $iniciativa;
		}
		return null;
	}

	public function delete($id) {
		$iniciativa = $this->findById($id);
		if ($iniciativa) {
			$iniciativa->delete();
			return true;
		}
		return false;
	}
}