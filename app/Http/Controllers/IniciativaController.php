<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Repositories\IniciativaRepository as Iniciativa;
use Request;

class IniciativaController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $iniciativa;

    public function __construct(Iniciativa $iniciativa) {
        $this->iniciativa = $iniciativa;
    }

    function list($id=null) {
    	if ($id == null) {
    		echo json_encode($this->iniciativa->findAll());
    	}
    	else {
    		echo json_encode($this->iniciativa->findById($id));
    	}
    }

    function save() {
    	$iniciativa = (Object) $_POST;
    	if (isset($iniciativa->id)) {
    		$result = $this->iniciativa->update($iniciativa->id,$iniciativa);
    	}
    	else {
	    	$result = $this->iniciativa->create($iniciativa);
	    }
    	echo json_encode($result);
    }

    function delete($id) {
		if ($this->iniciativa->delete($id)) {
			echo "ok";
		}
		else {
			echo "erro";
		}
    }
}
