<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIniciativaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iniciativa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('apresentador');
            $table->string('descricao');
            $table->string('tipo_usuario');
            $table->boolean('instituicao_cadastrada');
            $table->string('empresa');
            $table->timestamps();
        });

        Schema::create('iniciativa_passo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('iniciativa_id');
            $table->string('nome_meta');
            $table->string('criterios');
            $table->timestamps();
            $table->foreign('iniciativa_id')->references('id')->on('iniciativa')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iniciativa_passo');
        Schema::dropIfExists('iniciativa');
    }
}
